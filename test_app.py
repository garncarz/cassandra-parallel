import app


def test_ranges():
    s1, e1 = app.range_tokens(0)
    assert s1 == -9223372036854775807

    s2, e2 = app.range_tokens(1)
    assert e1 + 1 == s2

    s3, e3 = app.range_tokens(app.M - 1)
    assert e3 == 9223372036854775807
