# Multithreading query coordinator for Cassandra

Based on the [Efficient full table scans with Scylla 1.6](http://www.scylladb.com/2017/02/13/efficient-full-table-scans-with-scylla-1-6/) article.

Simply prints parallely found rows.


## Installation

Needed:

- Python 3
- Cassandra

Preferably under `virtualenv`:

`pip install pip-tools` (once)

`pip-sync requirements*.txt` (keeping the PyPI dependencies up-to-date)


## Configuration

Tweak `KEYSPACE` and `TABLE` variables in `app.py`.


## Usage

`./app.py`


## Testing

`pytest`