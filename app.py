#!/usr/bin/env python3

import queue
import threading

from cassandra.cluster import Cluster


KEYSPACE = 'ondra'
TABLE = 'example'


NODES_IN_CLUSTER = 1  # TODO dynamically find out
CORES_IN_NODE = 1  # TODO dynamically find out

N = NODES_IN_CLUSTER * CORES_IN_NODE * 3
M = N * 100


TOKEN_MIN = -(2**63 - 1)
TOKEN_MAX = 2**63 - 1

RANGE_SIZE = (TOKEN_MAX - TOKEN_MIN) // M


range_queue = queue.Queue()


def range_tokens(part):
    token_start = TOKEN_MIN + part * RANGE_SIZE
    # last part may be a bit bigger, because RANGE_SIZE is int
    token_end = TOKEN_MAX if part == M - 1 \
                else token_start + RANGE_SIZE - 1
    return token_start, token_end


def thread_print(msg):
    print('%s: %s' % (threading.current_thread().name, msg))


def process_range(session, token_start, token_end):
    rows = session.execute(
        'SELECT * FROM %s WHERE token(id) >= %d AND token(id) <= %d'
        % (TABLE, token_start, token_end)
    )
    for row in rows:
        thread_print(row)


def worker():
    cluster = Cluster()
    session = cluster.connect(KEYSPACE)

    while True:
        item = range_queue.get()
        if item is None:
            break

        token_start, token_end = item

        thread_print('%d..%d' % (token_start, token_end))

        process_range(session, token_start, token_end)
        range_queue.task_done()


def query_coordinator():
    for part in range(M):
        range_queue.put(range_tokens(part))

    nodes = []
    for node_nr in range(N):
        node = threading.Thread(target=worker)
        node.start()
        nodes.append(node)

    range_queue.join()

    for _ in range(N):
        range_queue.put(None)
    for node in nodes:
        node.join()


if __name__ == '__main__':
    query_coordinator()
